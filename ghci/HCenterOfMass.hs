module HCenterOfMass where

import Image
import Data.List

-- Student information:
--  Student 1
--    lastname:
--    student number:
--  Student 2
--    lastname:
--    student number:

-----------------------------------------------------------------------------------------
-- Assignment 1, From grayscale to black and white
-----------------------------------------------------------------------------------------
bfilter:: Int -> [Int] -> [Int]
bfilter _ [] = []
bfilter t (n:ns) 
    | n>t = 1 : bfilter t ns
    | n<=t = 0 :bfilter t ns



threshold :: Int -> [[Int]] -> [[Int]]
threshold _ [] = []
threshold t (g:gs)  = bfilter t g : threshold t gs
-----------------------------------------------------------------------------------------
-- Assignment 2, Center of mass of rows and picture
-----------------------------------------------------------------------------------------
vectorLen :: [Int] -> Int
vectorLen [] = 0
vectorLen x = foldl (\acc x -> acc+1) 0 x

vectorScalarProd:: [Int] -> [Int] -> Int
vectorScalarProd xs ys = w
 where
  w = foldl (+) 0 zs
  zs = zipWith (*) xs ys

comRows:: [[Int]] -> Int
comRows [] = 0
comRows g = let x = [1 .. vectorLen $ head g]
                y  = map sum g
                t = sum y
                is_img_black = t == 0
                z = vectorScalarProd x y
                out
                 | is_img_black  = div (vectorLen $ head g) 2
                 | otherwise = div z t
                in  out

comCol:: [[Int]] -> Int
comCol [] = 0
comCol g = let x = transpose g
                in  comRows x

imageWithCom :: Int -> [[Int]] -> [[Int]]
imageWithCom _ [[]] =[[]]
imageWithCom color image = newimg
 where
  binaryimg = threshold 125 image
  y = comCol binaryimg
  x = comRows binaryimg
  newimg = changePixelInImage image y x color

-----------------------------------------------------------------------------------------
-- Assignment 3 Center of mass of parts of the image, with and without borders
-----------------------------------------------------------------------------------------

comParts:: [[Int]] -> [[Int]]
comParts image = newimg 
 where
  binaryimg =threshold 127 image
  binaryimgBlocks = blocks2D 8 binaryimg
  imageBlocks = blocks2D 8 image
  xs_com = map (\x -> comRows x) binaryimgBlocks
  ys_com = map (\x -> comRows  (transpose x) ) binaryimgBlocks
  newimageBlocks = zipWith3 (\x y z -> changePixelInImage z (x-1) (y-1) 2) xs_com ys_com imageBlocks
  newimg = unblocks2D 128 newimageBlocks  


comPartsWB:: [[Int]] -> [[Int]]
comPartsWB image = newimg 
 where
  binaryimg =threshold 127 image
  binaryimgBlocks = addBorders 2 (blocks2D 8 binaryimg)
  imageBlocks = blocks2D 8 image
  xs_com = map (\x -> comRows x) binaryimgBlocks
  ys_com = map (\x -> comRows  (transpose x) ) binaryimgBlocks
  newimageBlocks = zipWith3 (\x y z -> changePixelInImage z (x-1) (y-1) 2) xs_com ys_com imageBlocks
  newimg = unblocks2D 128 newimageBlocks  
