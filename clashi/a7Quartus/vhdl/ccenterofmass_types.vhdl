library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package ccenterofmass_types is


  subtype maybe is std_logic_vector(32 downto 0);
  type tup2 is record
    tup2_sel0_boolean : boolean;
    tup2_sel1_maybe : ccenterofmass_types.maybe;
  end record;
  type array_of_unsigned_32 is array (integer range <>) of unsigned(31 downto 0);
  type tup2_0 is record
    tup2_0_sel0_maybe_0 : ccenterofmass_types.maybe;
    tup2_0_sel1_maybe_1 : ccenterofmass_types.maybe;
  end record;
  type array_of_array_of_8_unsigned_32 is array (integer range <>) of ccenterofmass_types.array_of_unsigned_32(0 to 7);
  subtype rst_system is std_logic;
  subtype clk_system is std_logic;
  function toSLV (b : in boolean) return std_logic_vector;
  function fromSLV (sl : in std_logic_vector) return boolean;
  function tagToEnum (s : in signed) return boolean;
  function dataToTag (b : in boolean) return signed;
  function toSLV (u : in unsigned) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return unsigned;
  function toSLV (slv : in std_logic_vector) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return std_logic_vector;
  function toSLV (p : ccenterofmass_types.tup2) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.tup2;
  function toSLV (value :  ccenterofmass_types.array_of_unsigned_32) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.array_of_unsigned_32;
  function toSLV (p : ccenterofmass_types.tup2_0) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.tup2_0;
  function toSLV (value :  ccenterofmass_types.array_of_array_of_8_unsigned_32) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.array_of_array_of_8_unsigned_32;
  function toSLV (sl : in std_logic) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return std_logic;
end;

package body ccenterofmass_types is
  function toSLV (b : in boolean) return std_logic_vector is
  begin
    if b then
      return "1";
    else
      return "0";
    end if;
  end;
  function fromSLV (sl : in std_logic_vector) return boolean is
  begin
    if sl = "1" then
      return true;
    else
      return false;
    end if;
  end;
  function tagToEnum (s : in signed) return boolean is
  begin
    if s = to_signed(0,64) then
      return false;
    else
      return true;
    end if;
  end;
  function dataToTag (b : in boolean) return signed is
  begin
    if b then
      return to_signed(1,64);
    else
      return to_signed(0,64);
    end if;
  end;
  function toSLV (u : in unsigned) return std_logic_vector is
  begin
    return std_logic_vector(u);
  end;
  function fromSLV (slv : in std_logic_vector) return unsigned is
  begin
    return unsigned(slv);
  end;
  function toSLV (slv : in std_logic_vector) return std_logic_vector is
  begin
    return slv;
  end;
  function fromSLV (slv : in std_logic_vector) return std_logic_vector is
  begin
    return slv;
  end;
  function toSLV (p : ccenterofmass_types.tup2) return std_logic_vector is
  begin
    return (toSLV(p.tup2_sel0_boolean) & toSLV(p.tup2_sel1_maybe));
  end;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.tup2 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 0)),fromSLV(islv(1 to 33)));
  end;
  function toSLV (value :  ccenterofmass_types.array_of_unsigned_32) return std_logic_vector is
    alias ivalue    : ccenterofmass_types.array_of_unsigned_32(1 to value'length) is value;
    variable result : std_logic_vector(1 to value'length * 32);
  begin
    for i in ivalue'range loop
      result(((i - 1) * 32) + 1 to i*32) := toSLV(ivalue(i));
    end loop;
    return result;
  end;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.array_of_unsigned_32 is
    alias islv      : std_logic_vector(0 to slv'length - 1) is slv;
    variable result : ccenterofmass_types.array_of_unsigned_32(0 to slv'length / 32 - 1);
  begin
    for i in result'range loop
      result(i) := fromSLV(islv(i * 32 to (i+1) * 32 - 1));
    end loop;
    return result;
  end;
  function toSLV (p : ccenterofmass_types.tup2_0) return std_logic_vector is
  begin
    return (toSLV(p.tup2_0_sel0_maybe_0) & toSLV(p.tup2_0_sel1_maybe_1));
  end;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.tup2_0 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 32)),fromSLV(islv(33 to 65)));
  end;
  function toSLV (value :  ccenterofmass_types.array_of_array_of_8_unsigned_32) return std_logic_vector is
    alias ivalue    : ccenterofmass_types.array_of_array_of_8_unsigned_32(1 to value'length) is value;
    variable result : std_logic_vector(1 to value'length * 256);
  begin
    for i in ivalue'range loop
      result(((i - 1) * 256) + 1 to i*256) := toSLV(ivalue(i));
    end loop;
    return result;
  end;
  function fromSLV (slv : in std_logic_vector) return ccenterofmass_types.array_of_array_of_8_unsigned_32 is
    alias islv      : std_logic_vector(0 to slv'length - 1) is slv;
    variable result : ccenterofmass_types.array_of_array_of_8_unsigned_32(0 to slv'length / 256 - 1);
  begin
    for i in result'range loop
      result(i) := fromSLV(islv(i * 256 to (i+1) * 256 - 1));
    end loop;
    return result;
  end;
  function toSLV (sl : in std_logic) return std_logic_vector is
  begin
    return std_logic_vector'(0 => sl);
  end;
  function fromSLV (slv : in std_logic_vector) return std_logic is
    alias islv : std_logic_vector (0 to slv'length - 1) is slv;
  begin
    return islv(0);
  end;
end;

