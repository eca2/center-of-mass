-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.ccenterofmass_types.all;

entity topentity is
  port(-- clock
       clk               : in ccenterofmass_types.clk_system;
       -- reset
       rst               : in ccenterofmass_types.rst_system;
       x_0_0             : in unsigned(31 downto 0);
       x_0_1             : in unsigned(31 downto 0);
       x_0_2             : in unsigned(31 downto 0);
       x_0_3             : in unsigned(31 downto 0);
       x_0_4             : in unsigned(31 downto 0);
       x_0_5             : in unsigned(31 downto 0);
       x_0_6             : in unsigned(31 downto 0);
       x_0_7             : in unsigned(31 downto 0);
       x_1_0             : in unsigned(31 downto 0);
       x_1_1             : in unsigned(31 downto 0);
       x_1_2             : in unsigned(31 downto 0);
       x_1_3             : in unsigned(31 downto 0);
       x_1_4             : in unsigned(31 downto 0);
       x_1_5             : in unsigned(31 downto 0);
       x_1_6             : in unsigned(31 downto 0);
       x_1_7             : in unsigned(31 downto 0);
       x_2_0             : in unsigned(31 downto 0);
       x_2_1             : in unsigned(31 downto 0);
       x_2_2             : in unsigned(31 downto 0);
       x_2_3             : in unsigned(31 downto 0);
       x_2_4             : in unsigned(31 downto 0);
       x_2_5             : in unsigned(31 downto 0);
       x_2_6             : in unsigned(31 downto 0);
       x_2_7             : in unsigned(31 downto 0);
       x_3_0             : in unsigned(31 downto 0);
       x_3_1             : in unsigned(31 downto 0);
       x_3_2             : in unsigned(31 downto 0);
       x_3_3             : in unsigned(31 downto 0);
       x_3_4             : in unsigned(31 downto 0);
       x_3_5             : in unsigned(31 downto 0);
       x_3_6             : in unsigned(31 downto 0);
       x_3_7             : in unsigned(31 downto 0);
       x_4_0             : in unsigned(31 downto 0);
       x_4_1             : in unsigned(31 downto 0);
       x_4_2             : in unsigned(31 downto 0);
       x_4_3             : in unsigned(31 downto 0);
       x_4_4             : in unsigned(31 downto 0);
       x_4_5             : in unsigned(31 downto 0);
       x_4_6             : in unsigned(31 downto 0);
       x_4_7             : in unsigned(31 downto 0);
       x_5_0             : in unsigned(31 downto 0);
       x_5_1             : in unsigned(31 downto 0);
       x_5_2             : in unsigned(31 downto 0);
       x_5_3             : in unsigned(31 downto 0);
       x_5_4             : in unsigned(31 downto 0);
       x_5_5             : in unsigned(31 downto 0);
       x_5_6             : in unsigned(31 downto 0);
       x_5_7             : in unsigned(31 downto 0);
       x_6_0             : in unsigned(31 downto 0);
       x_6_1             : in unsigned(31 downto 0);
       x_6_2             : in unsigned(31 downto 0);
       x_6_3             : in unsigned(31 downto 0);
       x_6_4             : in unsigned(31 downto 0);
       x_6_5             : in unsigned(31 downto 0);
       x_6_6             : in unsigned(31 downto 0);
       x_6_7             : in unsigned(31 downto 0);
       x_7_0             : in unsigned(31 downto 0);
       x_7_1             : in unsigned(31 downto 0);
       x_7_2             : in unsigned(31 downto 0);
       x_7_3             : in unsigned(31 downto 0);
       x_7_4             : in unsigned(31 downto 0);
       x_7_5             : in unsigned(31 downto 0);
       x_7_6             : in unsigned(31 downto 0);
       x_7_7             : in unsigned(31 downto 0);
       \c$tup_app_arg_0\ : out ccenterofmass_types.maybe;
       \c$tup_app_arg_1\ : out ccenterofmass_types.maybe);
end;

architecture structural of topentity is
  -- CCenterOfMass.hs:154:1-12
  signal s                             : boolean;
  -- CCenterOfMass.hs:154:1-12
  signal \res'1\                       : ccenterofmass_types.maybe;
  -- CCenterOfMass.hs:154:1-12
  signal ds                            : ccenterofmass_types.tup2;
  -- CCenterOfMass.hs:154:1-12
  signal res                           : ccenterofmass_types.maybe;
  signal result                        : unsigned(31 downto 0);
  -- CCenterOfMass.hs:100:1-13
  signal \partialResult0\              : ccenterofmass_types.array_of_unsigned_32(0 to 7);
  signal result_0                      : unsigned(31 downto 0);
  -- CCenterOfMass.hs:100:1-13
  signal \partialResult1\              : unsigned(31 downto 0);
  signal \c$app_arg\                   : unsigned(31 downto 0);
  signal \c$app_arg_0\                 : ccenterofmass_types.array_of_unsigned_32(0 to 7);
  signal \c$comRows_smallOut_app_arg\  : ccenterofmass_types.array_of_array_of_8_unsigned_32(0 to 7);
  signal \c$comRows_smallOut_case_alt\ : ccenterofmass_types.array_of_array_of_8_unsigned_32(0 to 7);
  signal x                             : ccenterofmass_types.array_of_array_of_8_unsigned_32(0 to 7);
  signal \c$vec1\                      : ccenterofmass_types.array_of_unsigned_32(0 to 7);
  signal \c$tup_app_arg\               : ccenterofmass_types.tup2_0;

begin
  x <= ccenterofmass_types.array_of_array_of_8_unsigned_32'( ccenterofmass_types.array_of_unsigned_32'( x_0_0
                                                                                                      , x_0_1
                                                                                                      , x_0_2
                                                                                                      , x_0_3
                                                                                                      , x_0_4
                                                                                                      , x_0_5
                                                                                                      , x_0_6
                                                                                                      , x_0_7 )
                                                           , ccenterofmass_types.array_of_unsigned_32'( x_1_0
                                                                                                      , x_1_1
                                                                                                      , x_1_2
                                                                                                      , x_1_3
                                                                                                      , x_1_4
                                                                                                      , x_1_5
                                                                                                      , x_1_6
                                                                                                      , x_1_7 )
                                                           , ccenterofmass_types.array_of_unsigned_32'( x_2_0
                                                                                                      , x_2_1
                                                                                                      , x_2_2
                                                                                                      , x_2_3
                                                                                                      , x_2_4
                                                                                                      , x_2_5
                                                                                                      , x_2_6
                                                                                                      , x_2_7 )
                                                           , ccenterofmass_types.array_of_unsigned_32'( x_3_0
                                                                                                      , x_3_1
                                                                                                      , x_3_2
                                                                                                      , x_3_3
                                                                                                      , x_3_4
                                                                                                      , x_3_5
                                                                                                      , x_3_6
                                                                                                      , x_3_7 )
                                                           , ccenterofmass_types.array_of_unsigned_32'( x_4_0
                                                                                                      , x_4_1
                                                                                                      , x_4_2
                                                                                                      , x_4_3
                                                                                                      , x_4_4
                                                                                                      , x_4_5
                                                                                                      , x_4_6
                                                                                                      , x_4_7 )
                                                           , ccenterofmass_types.array_of_unsigned_32'( x_5_0
                                                                                                      , x_5_1
                                                                                                      , x_5_2
                                                                                                      , x_5_3
                                                                                                      , x_5_4
                                                                                                      , x_5_5
                                                                                                      , x_5_6
                                                                                                      , x_5_7 )
                                                           , ccenterofmass_types.array_of_unsigned_32'( x_6_0
                                                                                                      , x_6_1
                                                                                                      , x_6_2
                                                                                                      , x_6_3
                                                                                                      , x_6_4
                                                                                                      , x_6_5
                                                                                                      , x_6_6
                                                                                                      , x_6_7 )
                                                           , ccenterofmass_types.array_of_unsigned_32'( x_7_0
                                                                                                      , x_7_1
                                                                                                      , x_7_2
                                                                                                      , x_7_3
                                                                                                      , x_7_4
                                                                                                      , x_7_5
                                                                                                      , x_7_6
                                                                                                      , x_7_7 ) );

  \c$tup_app_arg\ <= ( tup2_0_sel0_maybe_0 => res
                     , tup2_0_sel1_maybe_1 => \res'1\ ) when s else
                     ( tup2_0_sel0_maybe_0 => std_logic_vector'("0" & "--------------------------------")
                     , tup2_0_sel1_maybe_1 => std_logic_vector'("0" & "--------------------------------") );

  s <= ds.tup2_sel0_boolean;

  \res'1\ <= std_logic_vector'("1" & (std_logic_vector(result)));

  -- register begin
  topentity_register : block
    signal ds_reg : ccenterofmass_types.tup2 := ( tup2_sel0_boolean => false, tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(to_unsigned(0,32)))) );
  begin
    ds <= ds_reg; 
    ds_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ds_reg <= ( tup2_sel0_boolean => false, tup2_sel1_maybe => std_logic_vector'("1" & (std_logic_vector(to_unsigned(0,32)))) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ds_reg <= ( tup2_sel0_boolean => not s, tup2_sel1_maybe => \res'1\ )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end

  res <= ds.tup2_sel1_maybe;

  result <= result_0 when s else
            result_0;

  -- map begin
  map_r : for i_0 in \partialResult0\'range generate
  begin
    -- fold begin
    fold_2 : block
      -- given a level and a depth, calculate the corresponding index into the
      -- intermediate array
      function depth2index_2 (levels,depth : in natural) return natural is
      begin
        return (2 ** levels - 2 ** depth);
      end function;

      signal intermediate_2 : ccenterofmass_types.array_of_unsigned_32(0 to (2*8)-2);
      constant levels_2 : natural := natural (ceil (log2 (real (8))));
    begin
      -- put input array into the first half of the intermediate array
      intermediate_2(0 to 8-1) <= \c$comRows_smallOut_app_arg\(i_0);

      -- Create the tree of instantiated components
      make_tree_2 : if levels_2 /= 0 generate
        tree_depth_2 : for d_2 in levels_2-1 downto 0 generate
          tree_depth_loop_2 : for i_4 in 0 to (natural(2**d_2) - 1) generate
            intermediate_2(depth2index_2(levels_2+1,d_2+1)+i_4) <= intermediate_2(depth2index_2(levels_2+1,d_2+2)+(2*i_4)) + intermediate_2(depth2index_2(levels_2+1,d_2+2)+(2*i_4)+1);


          end generate;
        end generate;
      end generate;

      -- The last element of the intermediate array holds the result
      \partialResult0\(i_0) <= intermediate_2((2*8)-2);
    end block;
    -- fold end
  end generate;
  -- map end

  with (\partialResult1\) select
    result_0 <= to_unsigned(4,32) when x"00000000",
                \c$app_arg\ / \partialResult1\ when others;

  -- fold begin
  fold_0 : block
    -- given a level and a depth, calculate the corresponding index into the
    -- intermediate array
    function depth2index_0 (levels,depth : in natural) return natural is
    begin
      return (2 ** levels - 2 ** depth);
    end function;

    signal intermediate_0 : ccenterofmass_types.array_of_unsigned_32(0 to (2*8)-2);
    constant levels_0 : natural := natural (ceil (log2 (real (8))));
  begin
    -- put input array into the first half of the intermediate array
    intermediate_0(0 to 8-1) <= \partialResult0\;

    -- Create the tree of instantiated components
    make_tree_0 : if levels_0 /= 0 generate
      tree_depth_0 : for d_0 in levels_0-1 downto 0 generate
        tree_depth_loop_0 : for i_1 in 0 to (natural(2**d_0) - 1) generate
          intermediate_0(depth2index_0(levels_0+1,d_0+1)+i_1) <= intermediate_0(depth2index_0(levels_0+1,d_0+2)+(2*i_1)) + intermediate_0(depth2index_0(levels_0+1,d_0+2)+(2*i_1)+1);


        end generate;
      end generate;
    end generate;

    -- The last element of the intermediate array holds the result
    \partialResult1\ <= intermediate_0((2*8)-2);
  end block;
  -- fold end

  -- fold begin
  fold_1 : block
    -- given a level and a depth, calculate the corresponding index into the
    -- intermediate array
    function depth2index_1 (levels,depth : in natural) return natural is
    begin
      return (2 ** levels - 2 ** depth);
    end function;

    signal intermediate_1 : ccenterofmass_types.array_of_unsigned_32(0 to (2*8)-2);
    constant levels_1 : natural := natural (ceil (log2 (real (8))));
  begin
    -- put input array into the first half of the intermediate array
    intermediate_1(0 to 8-1) <= \c$app_arg_0\;

    -- Create the tree of instantiated components
    make_tree_1 : if levels_1 /= 0 generate
      tree_depth_1 : for d_1 in levels_1-1 downto 0 generate
        tree_depth_loop_1 : for i_2 in 0 to (natural(2**d_1) - 1) generate
          intermediate_1(depth2index_1(levels_1+1,d_1+1)+i_2) <= intermediate_1(depth2index_1(levels_1+1,d_1+2)+(2*i_2)) + intermediate_1(depth2index_1(levels_1+1,d_1+2)+(2*i_2)+1);


        end generate;
      end generate;
    end generate;

    -- The last element of the intermediate array holds the result
    \c$app_arg\ <= intermediate_1((2*8)-2);
  end block;
  -- fold end

  \c$vec1\ <= ccenterofmass_types.array_of_unsigned_32'( to_unsigned(1,32)
                                                       , to_unsigned(2,32)
                                                       , to_unsigned(3,32)
                                                       , to_unsigned(4,32)
                                                       , to_unsigned(5,32)
                                                       , to_unsigned(6,32)
                                                       , to_unsigned(7,32)
                                                       , to_unsigned(8,32) );

  -- zipWith begin
  zipwith : for i_3 in \c$app_arg_0\'range generate
  begin
    \c$app_arg_0\(i_3) <= resize(\c$vec1\(i_3) * \partialResult0\(i_3), 32);


  end generate;
  -- zipWith end

  \c$comRows_smallOut_app_arg\ <= \c$comRows_smallOut_case_alt\ when s else
                                  x;

  -- transpose begin
  transpose_outer : for row_index in 0 to (8 - 1) generate
    transpose_inner : for col_index in \c$comRows_smallOut_case_alt\'range generate
      \c$comRows_smallOut_case_alt\(col_index)(row_index) <= x(row_index)(col_index);
    end generate;
  end generate;
  -- transpose end

  \c$tup_app_arg_0\ <= \c$tup_app_arg\.tup2_0_sel0_maybe_0;

  \c$tup_app_arg_1\ <= \c$tup_app_arg\.tup2_0_sel1_maybe_1;


end;

