module Test where

import Clash.Prelude
import Image

type Picture = Vec 128 (Vec 128 Pix)
type PictureSmall = Vec 8 (Vec 8 Pix)


blocksOf8:: Picture -> Vec 16 (Vec 8 (Vec 8 Pix))


{--

Clash.Prelude> zipWith (\x y -> x !! y) ((1:>2:>Nil):>(2:>4:>Nil):>Nil) (0:>0:>Nil) 
<1,2>
Clash.Prelude> zipWith (\x y -> x !! y) ((1:>2:>Nil):>(2:>4:>Nil):>Nil) (01:>0:>Nil) 
<2,2>
Clash.Prelude> zipWith (\x y -> x !! y) ((1:>2:>Nil):>(2:>4:>Nil):>Nil) (1:>0:>Nil) 
<2,2>
Clash.Prelude> zipWith (\x y -> x !! y) ((1:>2:>Nil):>(2:>4:>Nil):>Nil) (1:>1:>Nil) 
<2,4>
Clash.Prelude> zipWith3 (\x y z -> x+2*y+3*z) (1:>2:>Nil) (1:>2:>Nil) (1:>2:>Nil)
<6,12>
Clash.Prelude> SNat (1) 

--}
