module CCenterOfMass where
import Clash.Prelude
import Image

-- Student information:
--  Student 1
--    lastname:
--    student number:
--  Student 2
--    lastname:
--    student number:


--- https://hackage.haskell.org/package/clash-prelude-0.10.14/docs/CLaSH-Sized-Vector.html

-----------------------------------------------------------------------------------------
-- Assignment 4, Changing a pixel in a picture
-----------------------------------------------------------------------------------------
type Clk = Clock System
type Rst = Reset System
type Sig = Signal System

type Pix = Unsigned 32
type PixDouble = Unsigned 32
type PictureBig = Vec 160 (Vec 160 Pix)
type Picture = Vec 128 (Vec 128 Pix)
type PictureSmall = Vec 8 (Vec 8 Pix)
type PictureMedium = Vec 10 (Vec 10 Pix)

--selectRow rowIndex a = head $ reverse  $ rotateLeft a rowIndex

changePixelInImage :: Pix -> Pix -> Pix -> Picture -> Picture
changePixelInImage val2change pixelRow pixelCol theimg = newimg 
  where
    row2change = head $ reverse  $ rotateLeft theimg pixelRow
    newrow = replace pixelCol val2change row2change
    newimg = replace pixelRow newrow theimg


changePixelInImageSmall :: Pix -> Pix -> Pix -> PictureSmall -> PictureSmall
changePixelInImageSmall val2change pixelRow pixelCol theimg = newimg 
  where
    row2change = head $ reverse  $ rotateLeft theimg pixelRow
    newrow = replace pixelCol val2change row2change
    newimg = replace pixelRow newrow theimg


changePixelInImageMedium :: Pix -> Pix -> Pix -> PictureMedium -> PictureMedium
changePixelInImageMedium val2change pixelRow pixelCol theimg = newimg 
  where
    row2change = head $ reverse  $ rotateLeft theimg pixelRow
    newrow = replace pixelCol val2change row2change
    newimg = replace pixelRow newrow theimg
 
-----------------------------------------------------------------------------------------
-- Assignment 5, Changing a pixel in a picture
-----------------------------------------------------------------------------------------


filter_function :: Pix -> Pix -> Pix
filter_function thresholdValue val
                | val >= thresholdValue = 1
                | val < thresholdValue = 0

binary_filter:: Pix -> Vec 128 Pix -> Vec 128 Pix
binary_filter thresholdValue vec = map (filter_function thresholdValue) vec



threshold:: Pix -> Picture -> Picture
threshold thresholdValue image = 
  map (binary_filter thresholdValue) image

sum_of_rows :: Picture -> Vec 128 Pix
sum_of_rows image = map (sum ) image

comRows :: Picture -> Pix
comRows image = result
         where 
             comCoef = scanl (+) 1 (replicate d127 1) -- generate the following array (1:> ... :> 128 :> Nil)
             partialResult1 = sum(sum_of_rows image)
             partialResult2 = sum (zipWith (*) comCoef $ sum_of_rows image)
             checkZero = partialResult1 == 0
             result 
                | checkZero = 64
                | otherwise = div partialResult2 partialResult1

comRowsDebug :: Picture -> (Pix,Pix)
comRowsDebug image = result
         where 
             comCoef = scanl (+) 1 (replicate d127 1) -- generate the following array (1:> ... :> 128 :> Nil)
             partialResult1 = sum(sum_of_rows image)
             partialResult2 = sum (zipWith (*) comCoef $ sum_of_rows image)
             result = (partialResult1,partialResult2)

-----------------------------------------------------------------------------------------
-- Assignment 6 Center of mass of parts of the image, with and without borders
-----------------------------------------------------------------------------------------
comRows_small :: PictureSmall -> Pix
comRows_small image = result
         where
             partialResult0 = map (sum) image
             partialResult1 = sum(partialResult0)
             partialResult2 = sum (zipWith (*) ((1:> 2:> 3:> 4:> 5:> 6:> 7:> 8:> Nil))  partialResult0)
             checkZero = partialResult1 == 0
             result 
                | checkZero = 4
                | otherwise = div partialResult2 partialResult1



comRows_medium :: PictureMedium -> Pix
comRows_medium image = result
         where
             partialResult0 = map (sum) image
             partialResult1 = sum(partialResult0)
             partialResult2 = sum (zipWith (*) ((1:> 2:> 3:> 4:> 5:> 6:> 7:> 8:> 9:> 10:> Nil))  partialResult0)
             checkZero = partialResult1 == 0
             result 
                | checkZero = 5
                | otherwise = div partialResult2 partialResult1


comParts:: Picture ->Picture
comParts image = newimg 
 where
  binaryimg =threshold 127 image
  binaryimgBlocks = blocks2D d8 binaryimg
  imageBlocks = blocks2D d8 image
  xs_com = map (\x -> comRows_small x) binaryimgBlocks
  ys_com = map (\x -> comRows_small  (transpose x) ) binaryimgBlocks
  newimageBlocks = zipWith3 (\x y z -> changePixelInImageSmall 2 (x-1) (y-1) z) xs_com ys_com imageBlocks
  newimg = unblocks2D d128 newimageBlocks



comPartsWB:: Picture ->PictureBig
comPartsWB image = newimg 
 where
  binaryimg =threshold 127 image
  binaryimgBlocks = addBorders 2 (blocks2D d8 binaryimg)
  imageBlocks = addBorders 2 (blocks2D d8 image)
  xs_com = map (\x -> comRows_medium x) binaryimgBlocks
  ys_com = map (\x -> comRows_medium  (transpose x) ) binaryimgBlocks
  newimageBlocks = zipWith3 (\x y z -> changePixelInImageMedium 2 (x-1) (y-1) z) xs_com ys_com imageBlocks
  newimg = unblocks2D d160 newimageBlocks 


-----------------------------------------------------------------------------------------
-- Assignment 7 Time-area trade-off
-----------------------------------------------------------------------------------------

imageWithCom:: (Bool, Maybe(Pix)) -> PictureSmall -> ( (Bool, Maybe(Pix)), (Maybe(Pix),Maybe(Pix))  )
imageWithCom (s,res) image = ((s', res') ,out)
   where 
    s' = not s
    image2process
       | s = transpose image
       | otherwise = image
    res' = Just (comRows_small) <*> (Just image2process)
    out
       | s = (res, res')
       | otherwise = (Nothing, Nothing)


imageWithComSim :: HiddenClockResetEnable dom  => Signal dom PictureSmall -> Signal dom (Maybe(Pix), Maybe(Pix))
imageWithComSim = mealy imageWithCom (False,(Just 0))


topEntity :: Clk -> Rst -> Sig PictureSmall -> Sig   ( Maybe(Pix), Maybe(Pix) )  
topEntity clk rst x = withClockResetEnable clk rst enableGen ( mealy imageWithCom (False,(Just 0)) ) x

--  bimg = threshold 127 image
--  blocks88 = blocks2D d8 bimg
--  blocks88Merged = merge blocks88 blocks88
--  simlist = toList blocks88Merged
-- simulate @System imageWithComSim simlist